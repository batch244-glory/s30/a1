db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "FruitsOnSale"}
]);


db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$count: "enoughStock"}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", avg_price: {$avg: "$price"}}}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", max_price: {$max: "$price"}}}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", min_price: {$min: "$price"}}}
]);